package com.example.ngailong.accelerationtestapp;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.LinkedList;

/*
 * An implementation to calculate standard deviation from a rolling window.
 *
 * @author Kaleb
 * @version %I%, %G%
 */
public class Std
{
    // The size of the sample window that determines RootMeanSquare Amplitude Noise
    // (standard deviation)
    // Refers to
    public final static int MAX_COUNT_EVENT = 10;

    private LinkedList<Double> list = new LinkedList<Double>();
    private double stdDev;
    private DescriptiveStatistics stats = new DescriptiveStatistics();

    /**
     * Add a sample to the rolling window.
     *
     * @param value
     *            The sample value.
     * @return The std dev of the rolling window.
     */
    public double addSample(double value)
    {
        list.addLast(value);

        replaceSample();

        return calculateStdDev();
    }

    /**
     * Enforce the rolling window.
     */
    private void replaceSample()
    {
        if (list.size() > MAX_COUNT_EVENT)
        {
            list.removeFirst();
        }
    }

    /**
     * Calculate the std dev of the rolling window.
     *
     * @return The std dev of the rolling window.
     */
    private double calculateStdDev()
    {
        if (list.size() > 5)
        {
            stats.clear();

            // Add the data from the array
            for (int i = 0; i < list.size(); i++)
            {
                stats.addValue(list.get(i));
            }

            stdDev = stats.getStandardDeviation();
        }

        return stdDev;
    }

}

