package com.example.ngailong.accelerationtestapp;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.List;

/*
While this implementation works well for short periods of time,
you run into a wall of sorts when trying to measure linear acceleration for extended periods of time.
This would require an extremely long time constant,
and the tilt compensation would take an extremely long time...
to the point where the purpose of the low-pass filter is defeated.
However, you can work around this by only applying the low-pass filter compensations
when the device is not experiencing
linear acceleration by analyzing the variance or magnitude (or both) of the acceleration sensor inputs.
This is beyond the scope of this article.
 */

public class MainActivity extends Activity implements SensorEventListener{

    //TODO: Simulate accelerometer's working principle in android phone

    private SensorManager mSensorManager;
    private List<Sensor> allSensor;

    private Sensor acceSensor;

    private TextView xtv;
    private TextView ytv;
    private TextView ztv;
    private TextView ftv;
    private TextView atv;
    private TextView gxtv;
    private TextView gytv;
    private TextView gztv;
    private TextView magtv;
    private TextView lin_magtv;
    private TextView lin_auto_magtv;

    private LPFAndroidDeveloper lowPassFilter;

    private float magnitude;

    private boolean filter_ready;
    private int count;

    // Times of data logged that exceed certain threshold
    // to be defined as an acceleration
    // The count number may vary due to frequency of the sensor
    private int minCountAsValidEvent = 10;
    private List<SensorEvent> validEvent;

    // Times of data logged that no longer exceed certain threshold
    // to be defined as an acceleration
    // The count number may vary due to frequency of the sensor
    private int minCountAsInvalidEvent = 10;
    private List<SensorEvent> invalidEvent;

    // Threshold minimum magnitude of acceleration that defined as a acceleration event
    private float minAccelerationInMagnitude = 3f;

    // Threshold max magnitude of acceleration that defined as an acceleration event
    private float maxAccelerationInMagnitude = 20f; // About 2G

    // Count of Acceleration Event Occur
    private int countAcce = 0;

    // Constants for the low-pass filters
    private float timeConstant = 0.04f;
    private float alpha;
    private float dt;

    // Timestamps for the low-pass filters
    private long timestampOld = System.nanoTime();

    // Gravity and linear accelerations components for the
// Wikipedia low-pass filter
    private float[] acceleration = new float[]
            { 0, 0, 0 };

    private float[] gravity = new float[]
            { 0, 0, 0 };

    private float[] linearAcceleration = new float[]
            { 0, 0, 0 };

    // Raw accelerometer data
    private float[] input = new float[]
            { 0, 0, 0 };
    private float thresholdMax = 0.5f; // = 0.5G
    private int thresholdCountMax;
    private int thresholdCountMaxLimit = 5; // five consecutive count
    private boolean acclerationEventActive;
    private Location location;
    private boolean locationEventAcquired;
    private double latitudeEvent;
    private double longitudeEvent;
    private float velocityEvent;
    private long timeEvent;
    private int thresholdCountMin;
    private float thresholdMin = 0.15f; // = 0.15G
    private int thresholdCountMinLimit = 10; // ten consecutive count

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        xtv = (TextView) findViewById(R.id.xtv);
        ytv = (TextView) findViewById(R.id.ytv);
        ztv = (TextView) findViewById(R.id.ztv);
        ftv = (TextView) findViewById(R.id.freq_tv);
        atv = (TextView) findViewById(R.id.atv);
        gxtv = (TextView) findViewById(R.id.gxtv);
        gytv = (TextView) findViewById(R.id.gytv);
        gztv = (TextView) findViewById(R.id.gztv);
        lin_magtv = (TextView) findViewById(R.id.line_magtv);

        //Initialize SensorManager
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        allSensor = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        Log.d("cnl", allSensor+"");
        acceSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        lowPassFilter = new LPFAndroidDeveloper();

    }

    @Override
    public void onSensorChanged(SensorEvent event) {


        if(timestampOld == 0) {
            timestampOld = event.timestamp;
        }

        // Find the sample period (between updates).
        // Convert from nanoseconds to seconds
        dt = ((event.timestamp - timestampOld) / 1000000000.0f);

        float frequency = 1 / dt;

        alpha = timeConstant / (timeConstant + dt);
        atv.setText(alpha+"");

        // TODO: frequency is not correct in this case
        // Set the frequency TextView
        ftv.setText(frequency + "");

        // Reset the time stamp
        timestampOld = event.timestamp;

        System.arraycopy(event.values, 0, acceleration, 0,
                event.values.length);

        // Change from m/s^-2 to fraction of G
        acceleration[0] = acceleration[0] / SensorManager.GRAVITY_EARTH;
        acceleration[1] = acceleration[1] / SensorManager.GRAVITY_EARTH;
        acceleration[2] = acceleration[2] / SensorManager.GRAVITY_EARTH;

        // Show raw data in UI in G unit
        xtv.setText(acceleration[0]+"");
        ytv.setText(acceleration[1]+"");
        ztv.setText(acceleration[2]+"");

        // Remove the gravity contribution with the high-pass filter.
        linearAcceleration = lowPassFilter.addSamples(acceleration);

        // Show low pass filtered linear acceleration data
        gxtv.setText(linearAcceleration[0]+"");
        gytv.setText(linearAcceleration[1]+"");
        gztv.setText(linearAcceleration[2]+"");

        magnitude = (float) Math
                .sqrt((Math.pow(linearAcceleration[0], 2)
                        + Math.pow(linearAcceleration[1], 2)
                        + Math.pow(linearAcceleration[2], 2)));

        // Show magnitude of linearAcceleration
        lin_magtv.setText(magnitude+"");

        if(!filter_ready){
            count++;
        }
        if(count == 50){
            filter_ready = true;
            TextView registrationTv = (TextView) findViewById(R.id.event_log);
            registrationTv.setText("The filter is ready to kick your ass!");
            //In order to escape from this infinite loop of count == 50
            count++;
        }

        if(magnitude > thresholdMax && filter_ready){
            thresholdCountMax++;
            Log.d("cnl", "ThresholdCountMax is " + thresholdCountMax + " times");
            // At least 5, in this case
            if(thresholdCountMax > thresholdCountMaxLimit){
                // Do the event registration

                acclerationEventActive = true; // Keep track of the accelerationEvent

                // Location is set in the location sensing model and recorded
                if(!locationEventAcquired){
                    if (this.location != null){ // Refers to the field var location
                        latitudeEvent = this.location.getLatitude();
                        longitudeEvent = this.location.getLongitude();
                        velocityEvent = this.location.getSpeed();
                        timeEvent = this.location.getTime();

                        //Should make use the the data in this bulk of data

                        locationEventAcquired = true;
                    }
                }

                TextView registrationTv = (TextView) findViewById(R.id.event_log);
                registrationTv.setText("Dangerous Event Recorded! Be Safe Driver!");
                // Reset after the event is registered
            }
        }
        else if(magnitude < thresholdMin && acclerationEventActive){
            thresholdCountMin++;

            Log.d("cnl", "ThresholdCountMin is " + thresholdCountMin + " times");
            // Get more than ten consecutive measurements below
            // threshold signal magnitude to end the acceleration event.
            if(thresholdCountMin > thresholdCountMinLimit){
                // When the acceleration event is stopeed
                // we cancel the locationEventAcquired
                // cancel the accelerationEventActive
                // Reset the minCount and maxCount
                locationEventAcquired = false;
                acclerationEventActive = false;

                thresholdCountMax = 0;
                thresholdCountMin = 0;

                TextView registrationTv = (TextView) findViewById(R.id.event_log);
                registrationTv.setText("Dangerous Event Stopped, please keep calm and stop driving.");
            }
        }
        else{
            // Get more than ten consecutive measurements below
            // Reset the counts in order to ensure the mesurements are really consecutive measurements!

            thresholdCountMax = 0;
            thresholdCountMin = 0;
            locationEventAcquired = false;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, acceSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public  void onAccuracyChanged(Sensor sensor, int accuracy){
        // Not used
    }


}
